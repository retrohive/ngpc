# SNK - Neo Geo Pocket Color

based on https://archive.org/details/no-intro_romsets (SNK - Neo Geo Pocket Color (20230129-060351))

ROM not added :
```sh
❯ tree -hs --dirsfirst                                                                                           sam. 04 févr. 2023 17:28:27
[4.0K]  .
├── [ 64K]  [BIOS] SNK Neo Geo Pocket Color (World) (En,Ja).ngp
└── [512K]  PP-AA01 - Pusher - Program - Hontai Controller Gawa (Japan).ngc

1 directory, 2 files
```
